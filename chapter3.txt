Rancangan System RICH Laundry 

Pada Rancangan System RICH Laundry ini ada 4 proses bisnis yang dilakukan:
1. Pengambilan Pakaian Kotor (Ardelia)
2. Proses Administrasi (Ardelia)
3. Pencucian dan Packing (Devita)
4. Pengantaran dan Pembayaran (Devita)

Pengambilan Pakaian Kotor
- Customer mengumpulkan baju yang ingin di laundry
- Customer menghubungi pihak laundry
- Pegawai Laundry merespond customer
- Customer memberitahukan bahwa ingin memberikan pakaian kotornya
- Pegawai Laundry menanyakan alamat customer
- Customer memberikan alamat
- Pegawai Laundry mengambil pakaian kotor di alamat customer
- Customer memberikan pakaian kotorya dan diberikan catatan bukti laundry

Proses Administrasi
- Pegawai Laundry sampai di tempat laundry
- Pegawai memberikan Laundry ke Admin yang bertugas
- Admin menimbang dan mencatat laundryan yang diberikan
- Pegawai yang mengambil laundry mendokumentasikan sebagai bukti berat untuk customer
- Pegawai mengirimkan bukti tersebut ke customer
- Customer menyetujui detail pencucian
- Pegawai yang berkomunikasi dengan customer menyampaikan ke Admin
- Admin memberikan catatan untuk Petugas yang mencuci

Untuk Pencucian dan Packing proses yang terjadi yaitu:
- Pakaian diterima oleh pencuci dan mulai disiapkan untuk dilakukan pencucian 
- Pencucian dilakukan dengan menggunakan deterjen serta pewangi terbaik dan berkualitas 
- Selesai dicuci pakaian mulai dikeringkan oleh pencuci menggunakan pengering terbaik
- Setelah dikeringkan pakaian mulai di setrika dan di harumkan menggunakan alat canggih yang tidak akan merusak baju 
- Langkah terakhir yaitu pakaian yang sudah selesai di laundry mulai di packing dengan plastik terbaik dan berkualitas 

Untuk Pengantaran dan Pembayaran proses yang terjadi yaitu:
- Kurir mulai mengambil pakaian yang telah selesai di laundry 
- Setelah itu kurir menuju alamat yang sesuai dengan alamat Customer 
- Sesampainya di tempat tujuan Customer mulai melakukan pembayaran sesuai dengan harga laundry tersebut 
- Lalu kurir mengambil uang tersebut sekaligus menyerahkan pakaian yang telah selesai di laundry 
- Customer mengambil pakaian tersebut 

SELESAI 
